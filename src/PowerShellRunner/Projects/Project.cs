using System.Collections.Generic;
using System.Linq;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.FileSystem;

namespace PowerShellRunner.Projects
{
    public class Project
    {
        internal const string Default = "default";

        private readonly Dictionary<string, Command> _commands;

        public static Project Load(string name, string path, IFileFinder fileFinder, IEnvironment environment)
        {
            var commands = fileFinder.FilesFrom($"{path}", "ps1")
                .Select(scriptFile => Command.Load(scriptFile, environment))
                .ToDictionary(command => command.Name);

            return new Project(commands);
        }

        private Project(Dictionary<string, Command> commands)
        {
            _commands = commands;
        }

        public string ScriptFor(string command)
        {
            return _commands[command].ScriptFile;
        }

        public bool TryGetCommand(string name, out Command command)
        {
            return _commands.TryGetValue(name, out command);
        }

        public IEnumerable<Command> Commands()
        {
            return _commands.Values;
        }

        public override string ToString()
        {
            return $"Loaded commands: {string.Join(",", _commands.Keys)}";
        }
    }
}