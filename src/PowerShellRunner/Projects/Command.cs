using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.FileSystem;
using PowerShellRunner.Help;

namespace PowerShellRunner.Projects
{
    public class Command
    {
        private const string Tab = "            ";
        private const string BaseCommand = "psr";
        private static readonly string[] CommonParameters = { "Verbose", "Debug", "ErrorAction", "ErrorVariable", "OutVariable", "OutBuffer", "WarningAction", "InformationAction", "WarningVariable", "InformationVariable", "PipelineVariable" };

        public Dictionary<string, Parameter> Parameters { get; }
        public Dictionary<string, string> Aliases { get; }

        public string ScriptFile => _file.FullName;
        public string Name => _file.Name;

        private readonly ICommandHelp _commandHelp;
        private readonly IFileInfo _file;

        public static Command Load(IFileInfo file, IEnvironment environment)
        {
            var aliases = new Dictionary<string, string>();
            var parameters = new Dictionary<string, Parameter>();

            var commandInfo = environment.GetCommandInfoFor(file.FullName);
            foreach (var parameter in commandInfo.Parameters)
            {
                if(CommonParameters.Contains(parameter.Name))
                    continue;

                var possibleValues = new List<string>();
                var valueFromRemainingArguments = false;
                foreach (var attribute in parameter.Attributes)
                {
                    switch (attribute)
                    {
                        case ValidateSetAttribute valueSetAttribute:
                            possibleValues.AddRange(valueSetAttribute.ValidValues);
                            break;
                        case ParameterAttribute parameterAttribute:
                            valueFromRemainingArguments = valueFromRemainingArguments || parameterAttribute.ValueFromRemainingArguments;
                            break;
                    }
                }

                if (valueFromRemainingArguments)
                    continue;

                foreach(var alias in parameter.Aliases)
                    aliases.Add(alias, parameter.Name);

                parameters.Add(parameter.Name, new Parameter
                {
                    HasValue = !parameter.SwitchParameter,
                    Aliases = parameter.Aliases.ToList(),
                    SetValues = possibleValues
                });
            }

            return new Command(file, parameters, aliases, commandInfo.Help);
        }

        public Command(IFileInfo file, Dictionary<string, Parameter> parameters, Dictionary<string, string> aliases, ICommandHelp commandHelp)
        {
            _file = file;
            Parameters = parameters;
            Aliases = aliases;
            _commandHelp = commandHelp;
        }

        public string GetHelp()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Usage: {BaseCommand} {_commandHelp.Name} [--help | -h]");
            stringBuilder.Append($"{Tab}");

            var counter = 0;
            foreach (var parameter in Parameters)
            {
                counter++;
                stringBuilder.Append($"[--{parameter.Key}");
                if (parameter.Value.HasValue)
                    stringBuilder.Append("=<value>");
                
                foreach (var alias in parameter.Value.Aliases)
                {
                    stringBuilder.Append($" | {alias}");

                    if (parameter.Value.HasValue)
                        stringBuilder.Append("=<value>");
                }

                stringBuilder.Append("] ");

                if (counter != 3)
                    continue;

                stringBuilder.AppendLine();
                stringBuilder.Append($"{Tab}");
                counter = 0;
            }

            stringBuilder.AppendLine();
            if (_commandHelp.Description != null)
            {
                stringBuilder.AppendLine();
                stringBuilder.AppendLine($"{_commandHelp.Description}");
            }

            stringBuilder.AppendLine();
            return stringBuilder.ToString();
        }
    }
}