namespace PowerShellRunner.Projects
{
    public interface IProjectCollection
    {
        void LoadCurrent();
        bool TryLoad(string name);
        string ScriptFor(string command);
        AllOptions GetOptionsFor(string project);
        AllOptions GetAllOptions();
        bool TryGetCommand(string name, out Command command, string project = null);
        bool IsKnownProject(string name);
    }
}