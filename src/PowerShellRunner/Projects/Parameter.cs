using System.Collections.Generic;

namespace PowerShellRunner.Projects
{
    public class Parameter
    {
        public List<string> Aliases { get; set; }
        public bool HasValue { get; set; }
        public List<string> SetValues { get; set; }

        public Parameter()
        {
            Aliases = new List<string>();
            SetValues = new List<string>();
        }
    }
}