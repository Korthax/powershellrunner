using System.Collections.Generic;
using System.Linq;

namespace PowerShellRunner.Projects
{
    public class AllOptions
    {
        public List<string> Commands { get;  set; }
        public List<string> Parameters { get; set; }
        public List<string> ValueParameters { get; set; }
        public List<string> Aliases { get; set; }
        public List<string> Projects { get; set; }

        internal static AllOptions Process(List<Command> commands, IEnumerable<string> projects)
        {
            return new AllOptions
            {
                Aliases = commands.SelectMany(x => x.Aliases.Keys).Distinct().OrderBy(x => x).ToList(),
                Parameters = commands.SelectMany(x => x.Parameters.Keys).Distinct().OrderBy(x => x).ToList(),
                ValueParameters = commands.SelectMany(x => x.Parameters).Where(x => x.Value.HasValue).Select(x => x.Key).Distinct().OrderBy(x => x).ToList(),
                Commands = commands.Select(x => x.Name).Distinct().OrderBy(x => x).ToList(),
                Projects = projects.Distinct().OrderBy(x => x).ToList()
            };
        }

        public AllOptions()
        {
            Commands = new List<string>();
            Parameters = new List<string>();
            Projects = new List<string>();
            ValueParameters = new List<string>();
            Aliases = new List<string>();
        }
    }
}