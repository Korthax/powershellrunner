using System;
using System.Collections.Generic;
using System.Linq;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.FileSystem;
using PowerShellRunner.Extensions;

namespace PowerShellRunner.Projects
{
    public class ProjectCollection : IProjectCollection
    {
        public const string ProjectKey = "PSR_Project_";

        private readonly Dictionary<string, Project> _loadedProjects;
        private readonly Dictionary<string, string> _knownProjects;
        private readonly IEnvironment _environment;
        private readonly IFileFinder _fileFinder;
        private readonly ILogger _logger;

        private string _current;

        public static ProjectCollection Initialise(IEnvironment environment, IFileFinder fileFinder, ILogger logger)
        {
            var knownProjects = new Dictionary<string, string>
            {
                [Project.Default] = environment.ScriptsLocation
            };

            foreach (var projectLocation in fileFinder.DirectoriesFor(environment.GetVariables($"{ProjectKey}")))
            {
                knownProjects.Add(projectLocation.ToProjectName(), projectLocation.FullName);
                logger.Info($"Loaded project '{projectLocation.ToProjectName()}'.");
            }

            var loadedProjects = new Dictionary<string, Project>
            {
                [Project.Default] = Project.Load(Project.Default, environment.ScriptsLocation, fileFinder, environment)
            };

            var collection = new ProjectCollection(environment, fileFinder, logger, Project.Default, knownProjects, loadedProjects);
            collection.LoadCurrent();
            return collection;
        }

        public ProjectCollection(IEnvironment environment, IFileFinder fileFinder, ILogger logger, string current, Dictionary<string, string> knownProjects, Dictionary<string, Project> loadedProjects)
        {
            _environment = environment;
            _fileFinder = fileFinder;
            _current = current;
            _knownProjects = knownProjects;
            _loadedProjects = loadedProjects;
            _logger = logger;
        }

        public void LoadCurrent()
        {
            var currentLocation = _environment.CurrentLocation();

            _current = _knownProjects
                .Where(knownProject => currentLocation.StartsWith(knownProject.Value, StringComparison.OrdinalIgnoreCase))
                .Select(x => x.Key)
                .FirstOrValue(Project.Default);

            _logger.Info($"Current project set to '{_current}'");

            if (_current == Project.Default || _loadedProjects.ContainsKey(_current))
                return;

            _loadedProjects.Add(_current, Project.Load(_current, _knownProjects[_current], _fileFinder, _environment));
        }

        public bool IsKnownProject(string name)
        {
            return _knownProjects.ContainsKey(name);
        }

        public bool TryLoad(string name)
        {
            if (!_knownProjects.ContainsKey(name))
            {
                _logger.Warning($"Trying to load unknown project '{name}'.");
                return false;
            }

            if (_loadedProjects.ContainsKey(name))
                return true;

            _loadedProjects.Add(name, Project.Load(name, _knownProjects[name], _fileFinder, _environment));
            return true;
        }

        public string ScriptFor(string command)
        {
            return _loadedProjects[_current].ScriptFor(command);
        }

        public bool TryGetCommand(string name, out Command command, string project = null)
        {
            if (!string.IsNullOrWhiteSpace(project))
            {
                if (!TryLoad(project))
                {
                    command = null;
                    return false;
                }

                if (_loadedProjects[project].TryGetCommand(name, out command))
                    return true;
            }
            else if (_current != Project.Default)
            {
                if (_loadedProjects[_current].TryGetCommand(name, out command))
                    return true;
            }

            return _loadedProjects[Project.Default].TryGetCommand(name, out command);
        }

        public AllOptions GetOptionsFor(string project)
        {
            var result = new List<Command>();
            result.AddRange(_loadedProjects[Project.Default].Commands());

            if (TryLoad(project))
            {
                if(!_loadedProjects.ContainsKey(project))
                    TryLoad(project);

                result.AddRange(_loadedProjects[project].Commands());
            }

            return AllOptions.Process(result, _knownProjects.Keys);
        }

        public AllOptions GetAllOptions()
        {
            var result = new List<Command>();
            result.AddRange(_loadedProjects[Project.Default].Commands());

            if (_current != Project.Default)
                result.AddRange(_loadedProjects[_current].Commands());

            return AllOptions.Process(result, _knownProjects.Keys);
        }
    }
}