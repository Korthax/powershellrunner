using System.Collections.Generic;
using System.Management.Automation;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.FileSystem;
using PowerShellRunner.Logging;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion;

namespace PowerShellRunner
{
    public class PowerShellRunner
    {
        public static void Run()
        {

        }

        public static IEnumerable<string> TabComplete(Cmdlet cmdlet, string line)
        {
            var logger = new PowerShellLogger(cmdlet);
             var instance = ProjectCollection.Initialise(new EnvironmentHelper(logger), new FileFinder(), logger);
            instance.LoadCurrent();
            var tabCompleter = new TabCompleter(instance);
            return tabCompleter.Complete(line);
        }
    }
}