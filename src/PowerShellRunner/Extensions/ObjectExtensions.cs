namespace PowerShellRunner.Extensions
{
    public static class ObjectExtensions
    {
        public static T As<T>(this object value) where T : class
        {
            return value as T;
        }
    }
}