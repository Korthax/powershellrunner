﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PowerShellRunner.Extensions
{
    public static class EnumerableExtensions
    {
        public static T FirstOrValue<T>(this IEnumerable<T> enumerable, T defaultValue) where T : class
        {
            return enumerable.FirstOrDefault() ?? defaultValue;
        }

        public static T FirstOrValue<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate, T defaultValue) where T : class
        {
            return enumerable.FirstOrDefault(predicate) ?? defaultValue;
        }
    }
}