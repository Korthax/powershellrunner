using System.Management.Automation;

namespace PowerShellRunner.Cmdlets
{
    [Cmdlet(VerbsCommon.Find, "MatchingScript")]
    [OutputType(typeof(string))]
    public class FindMatchingScript : PSCmdlet
    {
        [Parameter(ValueFromRemainingArguments = true)]
        public string Line { get; set; }

        protected override void ProcessRecord()
        {
            var result = PowerShellRunner.TabComplete(this, Line);
            WriteObject(result);
        }
    }
}