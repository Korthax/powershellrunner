using System.Management.Automation;
using PowerShellRunner.TabCompletion;

namespace PowerShellRunner.Cmdlets
{
    [Alias("psr")]
    [Cmdlet("Run", "PowerShellScript")]
    public class RunPowerShellScript : PSCmdlet
    {
        [Parameter(Position = 0, HelpMessage = "The line to match against")]
        public string Line { get; set; }

        protected override void BeginProcessing()
        {
            base.BeginProcessing();
        }

        protected override void ProcessRecord()
        {
        }
    }
}