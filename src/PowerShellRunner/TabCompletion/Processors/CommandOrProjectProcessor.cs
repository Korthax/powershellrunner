using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShellRunner.Projects;

namespace PowerShellRunner.TabCompletion.Processors
{
    public class CommandOrProjectProcessor : ITabCompleteCompletionProcessor
    {
        public static string HelpPattern => "^help (?<cmd>\\S*)$";
        public static string Pattern => "^(?<cmd>\\S*)$";

        private readonly IProjectCollection _projectCollection;

        public CommandOrProjectProcessor(IProjectCollection projectCollection)
        {
            _projectCollection = projectCollection;
        }

        public bool TryMatch(string line, out IEnumerable<string> matches)
        {
            var match = Regex.Match(line, Pattern);
            if (!match.Success)
            {
                match = Regex.Match(line, HelpPattern);
                if (!match.Success)
                {
                    matches = null;
                    return false;
                }
            }

            var command = match.Groups["cmd"].Value;
            var availableCommands = _projectCollection.GetAllOptions();
            matches = availableCommands.Commands.Where(x => x.StartsWith(command, StringComparison.OrdinalIgnoreCase));
            return true;
        }
    }
}