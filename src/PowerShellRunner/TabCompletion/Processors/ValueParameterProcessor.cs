using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShellRunner.Projects;

namespace PowerShellRunner.TabCompletion.Processors
{
    public class ValueParameterProcessor : ITabCompleteCompletionProcessor
    {
        public static string ProjectPattern => "^(?<proj>({0})\\S*).* (?<cmd>({1})).* --(?<param>[A-Za-z0-9]+)=(?<input>\\S*)$";
        public static string Pattern => "^(?<cmd>{0}).* --(?<param>[A-Za-z0-9]+)=(?<input>\\S*)$";
        private readonly IProjectCollection _projectCollection;

        public ValueParameterProcessor(IProjectCollection projectCollection)
        {
            _projectCollection = projectCollection;
        }

        public bool TryMatch(string line, out IEnumerable<string> matches)
        {
            var availableCommands = _projectCollection.GetAllOptions();

            var match = Regex.Match(line, string.Format(Pattern, string.Join("|", availableCommands.Commands)));
            if (!match.Success)
            {
                var pattern = string.Format(ProjectPattern, string.Join("|", availableCommands.Projects), string.Join("|", availableCommands.Commands));
                match = Regex.Match(line, pattern);
                if (!match.Success)
                {
                    matches = null;
                    return false;
                }
            }

            var parameter = match.Groups["param"].Value;
            var input = match.Groups["input"].Value;
            var project = match.Groups["proj"]?.Value;
            if (!_projectCollection.TryGetCommand(match.Groups["cmd"].Value, out var command, project))
            {
                matches = new string[0];
                return true;
            }

            var matchingParameter = command.Parameters
                .Where(x => x.Value.HasValue && x.Key.Equals(parameter, StringComparison.OrdinalIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();

            var matchingValues = matchingParameter?.SetValues
                .Where(x => x.StartsWith(input, StringComparison.OrdinalIgnoreCase))
                .ToList();

            if (matchingValues == null || matchingValues.Count == 0)
            {
                matches = new string[0];
                return true;
            }

            matches = matchingValues
                .Select(x => $"--{parameter}={x}");

            return true;
        }
    }
}