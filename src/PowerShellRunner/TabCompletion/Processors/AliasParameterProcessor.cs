using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShellRunner.Projects;

namespace PowerShellRunner.TabCompletion.Processors
{
    public class AliasParameterProcessor : ITabCompleteCompletionProcessor
    {
        public static string ProjectPattern => "^(?<proj>({0})\\S*).* (?<cmd>({1})).* -(?<shortparam>[A-Za-z0-9]\\S*)$";
        public static string Pattern => "^(?<cmd>({0})).* -(?<shortparam>[A-Za-z0-9]\\S*)$";

        private readonly IProjectCollection _projectCollection;

        public AliasParameterProcessor(IProjectCollection projectCollection)
        {
            _projectCollection = projectCollection;
        }

        public bool TryMatch(string line, out IEnumerable<string> matches)
        {
            var availableCommands = _projectCollection.GetAllOptions();

            var match = Regex.Match(line, string.Format(Pattern, string.Join("|", availableCommands.Commands)));
            if (!match.Success)
            {
                var pattern = string.Format(ProjectPattern, string.Join("|", availableCommands.Projects), string.Join("|", availableCommands.Commands));
                match = Regex.Match(line, pattern);
                if (!match.Success)
                {
                    matches = null;
                    return false;
                }
            }

            var alias = match.Groups["shortparam"];
            var project = match.Groups["proj"]?.Value;
            if (!_projectCollection.TryGetCommand(match.Groups["cmd"].Value, out var command, project))
            {
                var pattern = string.Format(ProjectPattern, string.Join("|", availableCommands.Projects), string.Join("|", availableCommands.Commands));
                match = Regex.Match(line, pattern);
                if (!match.Success)
                {
                    matches = new string[0];
                    return true;
                }
            }

            matches = command.Aliases
                .Where(x => x.Key.StartsWith(alias.Value, StringComparison.OrdinalIgnoreCase))
                .Select(x => command.Parameters[x.Value].HasValue
                    ? $"-{x.Key}="
                    : $"-{x.Key}"
                );

            return true;
        }
    }
}