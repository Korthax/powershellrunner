using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShellRunner.Projects;

namespace PowerShellRunner.TabCompletion.Processors
{
    public class ProjectCommandProcessor : ITabCompleteCompletionProcessor
    {
        public static string HelpPattern => "^help (?<proj>({0})\\S*).* (?<cmd>\\S*)$";
        public static string Pattern => "^(?<proj>({0})\\S*).* (?<cmd>\\S*)$";

        private readonly IProjectCollection _projectCollection;

        public ProjectCommandProcessor(IProjectCollection projectCollection)
        {
            _projectCollection = projectCollection;
        }

        public bool TryMatch(string line, out IEnumerable<string> matches)
        {
            var availableCommands = _projectCollection.GetAllOptions();

            var pattern = string.Format(Pattern, string.Join("|", availableCommands.Projects));
            var match = Regex.Match(line, pattern);
            if (!match.Success)
            {
                pattern = string.Format(HelpPattern, string.Join("|", availableCommands.Projects));
                match = Regex.Match(line, pattern);
                if (!match.Success)
                {
                    matches = null;
                    return false;
                }
            }

            var project = match.Groups["proj"].Value;
            var command = match.Groups["cmd"].Value;

            if (!_projectCollection.IsKnownProject(project))
            {
                matches = new List<string>();
                return true;
            }

            matches = availableCommands.Commands.Where(x => x.StartsWith(command, StringComparison.OrdinalIgnoreCase));
            return true;
        }
    }
}