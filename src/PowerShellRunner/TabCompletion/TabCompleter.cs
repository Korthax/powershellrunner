using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion.Processors;

namespace PowerShellRunner.TabCompletion
{
    public class TabCompleter
    {
        private readonly IEnumerable<ITabCompleteCompletionProcessor> _processors;
        private readonly IProjectCollection _projectCollection;

        public TabCompleter(IProjectCollection projectCollection)
        {
            _projectCollection = projectCollection;
            _processors = new ITabCompleteCompletionProcessor[]
            {
                new ValueParameterProcessor(projectCollection),
                new AliasParameterProcessor(projectCollection),
                new ParameterProcessor(projectCollection),
                new ProjectCommandProcessor(projectCollection),
                new CommandOrProjectProcessor(projectCollection),
            };
        }

        public IEnumerable<string> Complete(string line)
        {
            _projectCollection.LoadCurrent();

            var lastBlock = Regex.Split(line, "[|;]").Last().TrimStart();
            foreach (var tabCompletionResult in _processors)
            {
                if (tabCompletionResult.TryMatch(lastBlock, out var result))
                    return result.OrderBy(x => x);
            }

            return new List<string>();
        }
    }
}