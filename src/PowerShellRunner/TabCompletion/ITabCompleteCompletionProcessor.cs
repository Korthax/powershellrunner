using System.Collections.Generic;

namespace PowerShellRunner.TabCompletion
{
    public interface ITabCompleteCompletionProcessor
    {
        bool TryMatch(string line, out IEnumerable<string> matches);
    }
}