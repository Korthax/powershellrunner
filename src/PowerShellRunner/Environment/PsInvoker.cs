using System.Collections;
using System.Linq;
using System.Management.Automation;

namespace PowerShellRunner.Environment
{
    public class PsInvoker
    {
        public static PSObject[] InvokeCommand(string commandName, Hashtable parameters)
        {
            var scriptBlock = ScriptBlock.Create("param($Command, $Params) & $Command @Params");
            return scriptBlock.Invoke(commandName, parameters).ToArray();
        }
    }
}