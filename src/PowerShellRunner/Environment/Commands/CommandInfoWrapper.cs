using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Language;
using PowerShellRunner.Help;

namespace PowerShellRunner.Environment.Commands
{
    public class CommandInfoWrapper : ICommandInfo
    {
        public IEnumerable<IParameterMetadata> Parameters { get; }
        public ICommandHelp Help { get; }

        public static CommandInfoWrapper From(ExternalScriptInfo scriptInfo)
        {
            var ast = Parser.ParseInput(scriptInfo.ScriptContents, out var _, out var _);
            var help = HelpCommentsParser.GetHelp(ast);
            return new CommandInfoWrapper(scriptInfo.Parameters.Select(x => new ParameterMetadataWrapper(x.Value)).ToList(), help);
        }

        private CommandInfoWrapper(IEnumerable<IParameterMetadata> parameters, ICommandHelp help)
        {
            Parameters = parameters;
            Help = help;
        }
    }
}