using System.Collections.Generic;
using PowerShellRunner.Help;

namespace PowerShellRunner.Environment.Commands
{
    public class NullCommandInfo : ICommandInfo
    {
        public IEnumerable<IParameterMetadata> Parameters { get; }
        public ICommandHelp Help { get; }

        public NullCommandInfo()
        {
            Parameters = new List<IParameterMetadata>();
            Help = new NoHelp();
        }
    }
}