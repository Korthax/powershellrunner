using System;
using System.Collections.ObjectModel;

namespace PowerShellRunner.Environment.Commands
{
    public interface IParameterMetadata
    {
        Collection<string> Aliases { get; }
        Collection<Attribute> Attributes { get; }
        string Name { get; }
        Type ParameterType { get; }
        bool SwitchParameter { get; }
    }
}