using System.Collections.Generic;
using PowerShellRunner.Help;

namespace PowerShellRunner.Environment.Commands
{
    public interface ICommandInfo
    {
        IEnumerable<IParameterMetadata> Parameters { get; }
        ICommandHelp Help { get; }
    }
}