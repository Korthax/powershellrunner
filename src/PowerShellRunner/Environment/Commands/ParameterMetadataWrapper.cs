using System;
using System.Collections.ObjectModel;
using System.Management.Automation;

namespace PowerShellRunner.Environment.Commands
{
    public class ParameterMetadataWrapper : IParameterMetadata
    {
        public Collection<string> Aliases => _parameterMetadata.Aliases;
        public Collection<Attribute> Attributes => _parameterMetadata.Attributes;
        public string Name => _parameterMetadata.Name;
        public Type ParameterType => _parameterMetadata.ParameterType;
        public bool SwitchParameter => _parameterMetadata.SwitchParameter;

        private readonly ParameterMetadata _parameterMetadata;

        public ParameterMetadataWrapper(ParameterMetadata parameterMetadata)
        {
            _parameterMetadata = parameterMetadata;
        }
    }
}