using System.IO;

namespace PowerShellRunner.Environment.FileSystem
{
    public class DirectoryInfoWrapper : IDirectoryInfo
    {
        public string FullName => _directoryInfo.FullName;
        public string Name => _directoryInfo.Name;

        private readonly DirectoryInfo _directoryInfo;

        public DirectoryInfoWrapper(DirectoryInfo directoryInfo)
        {
            _directoryInfo = directoryInfo;
        }
    }
}