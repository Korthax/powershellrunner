﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PowerShellRunner.Environment.FileSystem
{
    public class FileFinder : IFileFinder
    {
        public IEnumerable<IDirectoryInfo> DirectoriesFor(List<KeyValuePair<string, string>> path)
        {
            return path
                .Where(x => Directory.Exists(x.Value))
                .Select(x => new DirectoryInfoWrapper(new DirectoryInfo(x.Value)))
                .ToList<IDirectoryInfo>();
        }

        public IEnumerable<IFileInfo> FilesFrom(string path, string fileType)
        {
            return Directory.GetFiles(path, fileType, SearchOption.AllDirectories)
                .Select(x => new FileInfoWrapper(new FileInfo(x)))
                .ToList<IFileInfo>();
        }
    }
}