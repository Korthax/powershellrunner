using PowerShellRunner.Projects;

namespace PowerShellRunner.Environment.FileSystem
{
    public static class DirectoryInfoExtensions
    {
        public static string ToProjectName(this IDirectoryInfo directoryInfo)
        {
            return directoryInfo.Name.Replace(ProjectCollection.ProjectKey, "").ToLower();
        }
    }
}