using System.Collections.Generic;

namespace PowerShellRunner.Environment.FileSystem
{
    public interface IFileFinder
    {
        IEnumerable<IDirectoryInfo> DirectoriesFor(List<KeyValuePair<string, string>> path);
        IEnumerable<IFileInfo> FilesFrom(string path, string fileType);
    }
}