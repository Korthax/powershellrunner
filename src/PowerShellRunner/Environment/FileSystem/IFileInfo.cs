namespace PowerShellRunner.Environment.FileSystem
{
    public interface IFileInfo
    {
        string Name { get; }
        string FullName { get; }
    }
}