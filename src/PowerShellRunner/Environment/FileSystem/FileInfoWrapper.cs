using System.IO;

namespace PowerShellRunner.Environment.FileSystem
{
    public class FileInfoWrapper : IFileInfo
    {
        public string FullName => _fileInfo.FullName;
        public string Name => _fileInfo.Name;

        private readonly FileInfo _fileInfo;

        public FileInfoWrapper(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }
    }
}