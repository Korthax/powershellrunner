namespace PowerShellRunner.Environment.FileSystem
{
    public interface IDirectoryInfo
    {
        string Name { get; }
        string FullName { get; }
    }
}