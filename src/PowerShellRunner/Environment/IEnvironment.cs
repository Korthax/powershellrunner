using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using PowerShellRunner.Environment.Commands;

namespace PowerShellRunner.Environment
{
    public interface IEnvironment
    {
        string ScriptsLocation { get; }
        List<KeyValuePair<string, string>> GetVariables(string pattern);
        string CurrentLocation();
        ICommandInfo GetCommandInfoFor(string path);
    }

    public class EnvironmentHelper : IEnvironment
    {
        public string ScriptsLocation => $"{System.Environment.GetEnvironmentVariable("PSR_HOME")}/scripts";

        private readonly ILogger _logger;

        public EnvironmentHelper(ILogger logger)
        {
            _logger = logger;
        }

        public List<KeyValuePair<string, string>> GetVariables(string pattern)
        {
            var result = new List<KeyValuePair<string, string>>();

            foreach (DictionaryEntry entry in System.Environment.GetEnvironmentVariables())
            {
                if(entry.Value.ToString().StartsWith(pattern))
                    result.Add(new KeyValuePair<string, string>(entry.Key.ToString(), entry.Value.ToString()));
            }

            return result;
        }

        public string CurrentLocation()
        {
            return Directory.GetCurrentDirectory();
        }

        public ICommandInfo GetCommandInfoFor(string path)
        {
            var result = PsInvoker.InvokeCommand("Get-Command", new Hashtable { { "Name", path } });
            var psObject = result?.FirstOrDefault();

            if (psObject != null)
            {
                var command = psObject.BaseObject as CommandInfo;

                if (command is AliasInfo alisInfo)
                    command = alisInfo.ResolvedCommand;

                if (command is ExternalScriptInfo externalScriptInfo)
                    return CommandInfoWrapper.From(externalScriptInfo);

                _logger.Error($"Cannot find command external script info for '{path}'.");
                return new NullCommandInfo();
            }

            _logger.Error($"Cannot find command info for '{path}'.");
            return new NullCommandInfo();
        }
    }
}