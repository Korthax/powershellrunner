﻿@{
    ModuleToProcess = 'PowerShellRunner.psm1'
    ModuleVersion = '0.0.0.0'
    GUID = '780e90be-3c9b-45bf-a5d0-e906626bd0ff'
    Author = 'Steve Phillips'
    Copyright = '(c) 2010-2018 Steve Phillips'
    Description = 'Run PowerShell scripts.'
    PowerShellVersion = '5.0'
    FunctionsToExport = @(
        'TabExpansion'
    )
    CmdletsToExport = @()
    VariablesToExport = @()
    AliasesToExport = @()
    PrivateData = @{
        PSData = @{
            Tags = @()
            LicenseUri = ''
            ProjectUri = ''
            ReleaseNotes = ''
        }
    }
}