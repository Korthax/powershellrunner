﻿using System;
using System.Management.Automation;
using PowerShellRunner.Environment;

namespace PowerShellRunner.Logging
{
    public class PowerShellLogger : ILogger
    {
        private readonly Cmdlet _cmdlet;

        public PowerShellLogger(Cmdlet cmdlet)
        {
            _cmdlet = cmdlet;
        }

        public void Info(string message)
        {
            _cmdlet.WriteDebug(message);
        }

        public void Warning(string message)
        {
            _cmdlet.WriteWarning(message);
        }

        public void Error(string message)
        {
            _cmdlet.WriteError(new ErrorRecord(new Exception(message), "0", ErrorCategory.ReadError, null));
        }
    }
}