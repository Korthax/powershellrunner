namespace PowerShellRunner.Help
{
    public class NoHelp : ICommandHelp
    {
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public string Description { get; set; }
    }
}