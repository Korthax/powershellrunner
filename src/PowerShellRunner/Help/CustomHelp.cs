namespace PowerShellRunner.Help
{
    public class CustomHelp : ICommandHelp
    {
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public string Description { get; set; }
    }
}