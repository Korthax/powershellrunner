using System.Collections.Generic;
using System.Management.Automation.Language;
using System.Text;
using System.Text.RegularExpressions;

namespace PowerShellRunner.Help
{
    internal class HelpCommentsParser
    {
        private const string Directive = @"^\s*\.(\w+)(\s+(\S.*))?\s*$";
        private const string Blankline = @"^\s*$";

        public static ICommandHelp GetHelp(Ast ast)
        {
            var rootAst = ast;
            while (rootAst.Parent != null)
                rootAst = rootAst.Parent;

            Parser.ParseInput(rootAst.Extent.Text, out var tokens, out _);

            var startTokenIndex = ast == rootAst ? 0 : FirstTokenInExtent(tokens, ast.Extent) - 1;
            while (true)
            {
                var commentBlock = GetCommentBlock(tokens, ref startTokenIndex);
                if (commentBlock.Count == 0)
                    break;

                if (TryGetHelp(commentBlock, out var help))
                    return help;
            }

            return new NoHelp();
        }

        private static bool TryGetHelp(IReadOnlyCollection<Token> commentBlock, out ICommandHelp help)
        {
            if (commentBlock == null || commentBlock.Count == 0)
            {
                help = null;
                return false;
            }

            var commentLines = new List<string>();
            foreach (var comment in commentBlock)
            {
                var i = 0;
                if (comment.Text[0] == '<')
                {
                    var start = 2;
                    for (i = 2; i < comment.Text.Length - 2; i++)
                    {
                        switch (comment.Text[i])
                        {
                            case '\n':
                                commentLines.Add(comment.Text.Substring(start, i - start));
                                start = i + 1;
                                break;
                            case '\r':
                                commentLines.Add(comment.Text.Substring(start, i - start));

                                if (comment.Text[i + 1] == '\n')
                                    i++;

                                start = i + 1;
                                break;
                        }
                    }

                    commentLines.Add(comment.Text.Substring(start, i - start));
                }
                else
                {
                    for (; i < comment.Text.Length; i++)
                    {
                        if (comment.Text[i] != '#')
                            break;
                    }

                    commentLines.Add(comment.Text.Substring(i));
                }
            }

            help = new CustomHelp();
            for (var i = 0; i < commentLines.Count; i++)
            {
                var match = Regex.Match(commentLines[i], Directive);
                if (match.Success)
                {
                    if (match.Groups[3].Success)
                        continue;

                    switch (match.Groups[1].Value.ToUpperInvariant())
                    {
                        case "DESCRIPTION":
                            help.Description = GetSection(commentLines, ref i).Trim(' ', '\t', '\n');
                            break;
                        case "SYNOPSIS":
                            help.Synopsis = GetSection(commentLines, ref i).Trim(' ', '\t', '\n');
                            break;
                        default:
                            GetSection(commentLines, ref i);
                            break;
                    }
                }
                else if (!Regex.IsMatch(commentLines[i], Blankline))
                    return true;
            }
            
            return true;
        }

        private static string GetSection(IReadOnlyList<string> commentLines, ref int i)
        {
            const char nbsp = (char)0xA0;

            var capturing = false;
            var countLeadingWs = 0;
            var stringBuilder = new StringBuilder();

            for (i++; i < commentLines.Count; i++)
            {
                var line = commentLines[i];
                if (!capturing && Regex.IsMatch(line, Blankline))
                    continue;

                if (Regex.IsMatch(line, Directive))
                {
                    i--;
                    break;
                }

                if (!capturing)
                {
                    var j = 0;
                    while (j < line.Length && (line[j] == ' ' || line[j] == '\t' || line[j] == nbsp))
                    {
                        countLeadingWs++;
                        j++;
                    }
                }

                capturing = true;

                var start = 0;
                while (start < line.Length && start < countLeadingWs && (line[start] == ' ' || line[start] == '\t' || line[start] == nbsp))
                    start++;

                stringBuilder.Append(line.Substring(start));
            }

            return stringBuilder.ToString();
        }

        private static List<Token> GetCommentBlock(IReadOnlyList<Token> tokens, ref int startIndex)
        {
            var result = new List<Token>();

            var nextMaxStartLine = int.MaxValue;
            for (var i = startIndex; i < tokens.Count; i++)
            {
                var current = tokens[i];
                if (current.Extent.StartLineNumber > nextMaxStartLine)
                {
                    startIndex = i;
                    break;
                }

                if (current.Kind == TokenKind.Comment)
                {
                    result.Add(current);
                    nextMaxStartLine = current.Extent.EndLineNumber + 1;
                }
                else if (current.Kind != TokenKind.NewLine)
                {
                    startIndex = i;
                    break;
                }
            }

            return result;
        }

        private static int FirstTokenInExtent(IReadOnlyList<Token> tokens, IScriptExtent extent, int startIndex = 0)
        {
            int index;
            for (index = startIndex; index < tokens.Count; ++index)
            {
                if (tokens[index].Extent.EndLineNumber < extent.StartLineNumber)
                    break;

                if (tokens[index].Extent.EndLineNumber == extent.StartLineNumber && tokens[index].Extent.EndColumnNumber <= extent.StartColumnNumber)
                    break;
            }

            return index;
        }
    }
}