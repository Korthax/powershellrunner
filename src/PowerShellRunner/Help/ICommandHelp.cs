namespace PowerShellRunner.Help
{
    public interface ICommandHelp
    {
        string Name { get; set; }
        string Synopsis { get; set; }
        string Description { get; set; }
    }
}