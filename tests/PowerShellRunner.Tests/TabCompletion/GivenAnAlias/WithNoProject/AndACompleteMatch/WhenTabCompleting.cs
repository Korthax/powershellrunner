using System.Collections.Generic;
using Moq;
using Xunit;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion;
using Shouldly;

namespace PowerShellRunner.Tests.TabCompletion.GivenAnAlias.WithNoProject.AndACompleteMatch
{
    public class WhenTabCompleting
    {
        private TabCompleter _tabCompleter;

        [Fact]
        public void ThenOnlyTheMatchingCommandIsReturned()
        {
            var allOptions = new AllOptions
            {
                Parameters = new List<string> { "param1", "param2", "aram", "ram" },
                Aliases = new List<string> { "aaa", "abc", "bcb" },
                Commands = new List<string> { "com1", "com2", "om", "m" },
                ValueParameters = new List<string> { "value1", "value2", "alue", "lue" }
            };

            var parameters = new Dictionary<string, Parameter>
            {
                ["aram"] = new Parameter {  Aliases = new List<string> { "aaa", "abc" }, HasValue = false }
            };

            var aliases = new Dictionary<string, string>
            {
                ["aaa"] = "aram",
                ["abc"] = "aram"
            };

            var command = new Command(null, parameters, aliases, null);

            var projectCollection = new Mock<IProjectCollection>();
            projectCollection.Setup(x => x.GetAllOptions()).Returns(allOptions);
            projectCollection.Setup(x => x.TryGetCommand("com1", out command, "")).Returns(true);

            _tabCompleter = new TabCompleter(projectCollection.Object);

            var result = _tabCompleter.Complete("com1 -ab");
           result.ShouldBe(new List<string> { "-abc" });
        }
    }
}