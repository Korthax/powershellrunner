using System.Collections.Generic;
using Moq;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.TabCompletion.GivenAParameter.WithAKnownProject.AndACompleteValueMatch
{
    public class WhenTabCompleting
    {
        private TabCompleter _tabCompleter;

        [Fact]
        public void ThenOnlyTheMatchingCommandIsReturned()
        {
            var allOptions = new AllOptions
            {
                Parameters = new List<string> { "param1", "param2", "aram", "ram" },
                Aliases = new List<string> { "a", "ab", "b" },
                Commands = new List<string> { "com1", "com2", "om", "m" },
                ValueParameters = new List<string> { "value1", "value2", "alue", "aram" },
                Projects = new List<string> { "default", "apr" }
            };

            var parameters = new Dictionary<string, Parameter>
            {
                ["aram"] = new Parameter { Aliases = new List<string>(), HasValue = true }
            };

            var command = new Command(null, parameters, new Dictionary<string, string>(), null);

            var projectCollection = new Mock<IProjectCollection>();
            projectCollection.Setup(x => x.GetAllOptions()).Returns(allOptions);
            projectCollection.Setup(x => x.TryGetCommand("com1", out command, "apr")).Returns(true);

            _tabCompleter = new TabCompleter(projectCollection.Object);

            var result = _tabCompleter.Complete("apr com1 --aram");
           result.ShouldBe(new List<string> { "--aram=" });
        }
    }
}