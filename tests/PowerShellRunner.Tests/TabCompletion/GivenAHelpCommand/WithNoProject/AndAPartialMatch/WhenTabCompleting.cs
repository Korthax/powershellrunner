using System.Collections.Generic;
using Moq;
using Xunit;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion;
using Shouldly;

namespace PowerShellRunner.Tests.TabCompletion.GivenAHelpCommand.WithNoProject.AndAPartialMatch
{
    public class WhenTabCompleting
    {
        private TabCompleter _tabCompleter;

        [Fact]
        public void ThenTheMatchingCommandsAreReturned()
        {
            var allOptions = new AllOptions
            {
                Parameters = new List<string> { "param1", "param2", "aram", "ram" },
                Aliases = new List<string> { "a", "ab", "b" },
                Commands = new List<string> { "com1", "com2", "om", "m" },
                ValueParameters = new List<string> { "value1", "value2", "alue", "lue" },
                Projects = new List<string> { "default" }
            };

            var projectCollection = new Mock<IProjectCollection>();
            projectCollection.Setup(x => x.GetAllOptions()).Returns(allOptions);

            _tabCompleter = new TabCompleter(projectCollection.Object);

            var result = _tabCompleter.Complete("help com");
           result.ShouldBe(new List<string> { "com1", "com2" });
        }
    }
}
