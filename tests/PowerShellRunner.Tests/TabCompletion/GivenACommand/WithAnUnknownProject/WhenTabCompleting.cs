using Moq;
using Xunit;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion;
using Shouldly;

namespace PowerShellRunner.Tests.TabCompletion.GivenACommand.WithAnUnknownProject
{
    public class WhenTabCompleting
    {
        private TabCompleter _tabCompleter;

        [Fact]
        public void SetThenAnEmptyResultIsReturnedUp()
        {
            var projectCollection = new Mock<IProjectCollection>();
            projectCollection.Setup(x => x.GetAllOptions()).Returns(new AllOptions());
            projectCollection.Setup(x => x.IsKnownProject("apr")).Returns(false);

            _tabCompleter = new TabCompleter(projectCollection.Object);

            var result = _tabCompleter.Complete("apr anything");
            result.ShouldBeEmpty();
        }
    }
}
