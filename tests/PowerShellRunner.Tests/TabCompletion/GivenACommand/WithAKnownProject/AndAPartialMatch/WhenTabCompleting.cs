using System.Collections.Generic;
using Moq;
using PowerShellRunner.Projects;
using PowerShellRunner.TabCompletion;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.TabCompletion.GivenACommand.WithAKnownProject.AndAPartialMatch
{
    public class WhenTabCompleting
    {
        private TabCompleter _tabCompleter;

        [Fact]
        public void ThenTheMatchingCommandsAreReturned()
        {
            var allOptions = new AllOptions
            {
                Parameters = new List<string> { "param1", "param2", "aram", "ram" },
                Aliases = new List<string> { "a", "ab", "b" },
                Commands = new List<string> { "com1", "com2", "om", "m" },
                ValueParameters = new List<string> { "value1", "value2", "alue", "lue" },
                Projects = new List<string> { "default", "apr" }
            };

            var projectCollection = new Mock<IProjectCollection>();
            projectCollection.Setup(x => x.GetAllOptions()).Returns(allOptions);
            projectCollection.Setup(x => x.GetOptionsFor("apr")).Returns(allOptions);
            projectCollection.Setup(x => x.IsKnownProject("apr")).Returns(true);

            _tabCompleter = new TabCompleter(projectCollection.Object);
            var result = _tabCompleter.Complete("apr com");
            result.ShouldBe(new List<string> { "com1", "com2" });
        }
    }
}
