﻿using System.Collections.Generic;
using Moq;
using PowerShellRunner.Environment.FileSystem;
using PowerShellRunner.Help;
using PowerShellRunner.Projects;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Help.GivenACommand.WithADescription
{
    public class WhenGettingTheHelp
    {
        [Fact]
        public void ThenThenTheCorrectHelpStringIsReturned()
        {
            var parameters = new Dictionary<string, Parameter>
            {
                ["param1"] = new Parameter { HasValue = true, Aliases = new List<string> { "p" } },
                ["alias"] = new Parameter { HasValue = false, Aliases = new List<string> { "a" } },
                ["abc"] = new Parameter { HasValue = false, Aliases = new List<string>() },
                ["koppo"] = new Parameter { HasValue = true, Aliases = new List<string>() },
                ["oo"] = new Parameter { HasValue = false, Aliases = new List<string>() },
                ["param2"] = new Parameter { HasValue = false, Aliases = new List<string>() },
                ["pri"] = new Parameter { HasValue = true, Aliases = new List<string>() }
            };

            var help = new Mock<ICommandHelp>();
            help.Setup(x => x.Name).Returns("build");
            help.Setup(x => x.Description).Returns("This is a description which has\r\nmultiple lines");

            var subject = new Command(new Mock<IFileInfo>().Object, parameters, new Dictionary<string, string>(), help.Object);
            var result = subject.GetHelp();

            result.ShouldBe(
@"Usage: psr build [--help | -h]
            [--param1=<value> | p=<value>] [--alias | a] [--abc] 
            [--koppo=<value>] [--oo] [--param2] 
            [--pri=<value>] 

This is a description which has
multiple lines

"
            );
        }
    }
}