﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Management.Automation;
using Moq;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.Commands;
using PowerShellRunner.Environment.FileSystem;
using PowerShellRunner.Projects;

namespace PowerShellRunner.Tests.TestHelpers
{
    public static class TestProject
    {
        public static ProjectCollection New(string currentLocation = "C:/")
        {
            var environmentVariables = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("PSR_Project_Opt", "C:/projects/opt"),
                new KeyValuePair<string, string>("PSR_Project_Arg", "C:/projects/arg")
            };

            var optProject = new Mock<IDirectoryInfo>();
            optProject.SetupGet(x => x.Name).Returns("opt");
            optProject.SetupGet(x => x.FullName).Returns("C:/projects/opt");

            var argProject = new Mock<IDirectoryInfo>();
            argProject.SetupGet(x => x.Name).Returns("arg");
            argProject.SetupGet(x => x.FullName).Returns("C:/projects/arg");

            var buildScriptInfo = new Mock<IFileInfo>();
            buildScriptInfo.SetupGet(x => x.Name).Returns("build");
            buildScriptInfo.SetupGet(x => x.FullName).Returns("C:/projects/psr/scripts/build.ps1");

            var deployScriptInfo = new Mock<IFileInfo>();
            deployScriptInfo.SetupGet(x => x.Name).Returns("deploy");
            deployScriptInfo.SetupGet(x => x.FullName).Returns("C:/projects/arg/scripts/deploy.ps1");

            var danceScriptInfo = new Mock<IFileInfo>();
            danceScriptInfo.SetupGet(x => x.Name).Returns("dance");
            danceScriptInfo.SetupGet(x => x.FullName).Returns("C:/projects/opt/scripts/dance.ps1");

            var nameParameter = new Mock<IParameterMetadata>();
            nameParameter.SetupGet(x => x.Name).Returns("Name");
            nameParameter.SetupGet(x => x.SwitchParameter).Returns(false);
            nameParameter.SetupGet(x => x.Aliases).Returns(new Collection<string> { "n" });
            nameParameter.SetupGet(x => x.Attributes).Returns(new Collection<Attribute>());

            var quickParameter = new Mock<IParameterMetadata>();
            quickParameter.SetupGet(x => x.Name).Returns("Quick");
            quickParameter.SetupGet(x => x.SwitchParameter).Returns(true);
            quickParameter.SetupGet(x => x.Aliases).Returns(new Collection<string>());
            quickParameter.SetupGet(x => x.Attributes).Returns(new Collection<Attribute>());

            var locationParameter = new Mock<IParameterMetadata>();
            locationParameter.SetupGet(x => x.Name).Returns("Location");
            locationParameter.SetupGet(x => x.SwitchParameter).Returns(false);
            locationParameter.SetupGet(x => x.Aliases).Returns(new Collection<string> { "l", "loc" });
            locationParameter.SetupGet(x => x.Attributes).Returns(new Collection<Attribute>
            {
                new ValidateSetAttribute("Google", "Azure")
            });

            var restParameter = new Mock<IParameterMetadata>();
            restParameter.SetupGet(x => x.Name).Returns("Args");
            restParameter.SetupGet(x => x.SwitchParameter).Returns(false);
            restParameter.SetupGet(x => x.Aliases).Returns(new Collection<string> { "l", "loc" });
            restParameter.SetupGet(x => x.Attributes).Returns(new Collection<Attribute>
            {
                new ParameterAttribute { ValueFromRemainingArguments = true }
            });

            var buildCommand = new Mock<ICommandInfo>();
            buildCommand.SetupGet(x => x.Parameters).Returns(new List<IParameterMetadata>
            {
                nameParameter.Object
            });

            var deployCommand = new Mock<ICommandInfo>();
            deployCommand.SetupGet(x => x.Parameters).Returns(new List<IParameterMetadata>
            {
                nameParameter.Object,
                quickParameter.Object,
                locationParameter.Object
            });

            var danceCommand = new Mock<ICommandInfo>();
            danceCommand.SetupGet(x => x.Parameters).Returns(new List<IParameterMetadata>
            {
                nameParameter.Object,
                restParameter.Object
            });

            var environment = new Mock<IEnvironment>();
            environment.Setup(x => x.GetVariables("PSR_Project_")).Returns(environmentVariables);
            environment.SetupGet(x => x.ScriptsLocation).Returns("C:/projects/psr");
            environment.Setup(x => x.GetCommandInfoFor("C:/projects/psr/scripts/build.ps1")).Returns(buildCommand.Object);
            environment.Setup(x => x.GetCommandInfoFor("C:/projects/arg/scripts/deploy.ps1")).Returns(deployCommand.Object);
            environment.Setup(x => x.GetCommandInfoFor("C:/projects/opt/scripts/dance.ps1")).Returns(danceCommand.Object);
            environment.Setup(x => x.CurrentLocation()).Returns(currentLocation);

            var fileFinder = new Mock<IFileFinder>();
            fileFinder.Setup(x => x.FilesFrom("C:/projects/psr", "ps1")).Returns(new List<IFileInfo> { buildScriptInfo.Object });
            fileFinder.Setup(x => x.FilesFrom("C:/projects/arg", "ps1")).Returns(new List<IFileInfo> { deployScriptInfo.Object });
            fileFinder.Setup(x => x.FilesFrom("C:/projects/opt", "ps1")).Returns(new List<IFileInfo> { danceScriptInfo.Object });
            fileFinder.Setup(x => x.DirectoriesFor(environmentVariables)).Returns(new List<IDirectoryInfo> { optProject.Object, argProject.Object });

            return ProjectCollection.Initialise(environment.Object, fileFinder.Object, new Mock<ILogger>().Object);
        }
    }
}