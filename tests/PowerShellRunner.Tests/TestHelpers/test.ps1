﻿<#
.Synopsis
    Deploys because Steve is the best
.Description
    Deploys because Steve is the best
#>
[CmdletBinding()]
Param(
    [Parameter(Position = 0, Mandatory = $true)]
    [string] $Name,
    [Parameter(Position = 1, Mandatory = $true)]
    [switch]$DontFail
)
Process {
    <#
        Random comment that spans
        multiple lines
    #>
    Write-Host "Deploy" -ForegroundColor Yellow
}