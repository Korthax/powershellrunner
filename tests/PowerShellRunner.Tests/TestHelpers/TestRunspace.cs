﻿using System;
using System.Management.Automation.Runspaces;

namespace PowerShellRunner.Tests.TestHelpers
{
    public class TestRunspace : IDisposable
    {
        private readonly Runspace _runspace;

        public static TestRunspace Create()
        {
            var runspace = RunspaceFactory.CreateRunspace();
            runspace.Open();
            Runspace.DefaultRunspace = runspace;

            var pipeline = runspace.CreatePipeline();
            pipeline.Commands.AddScript("Set-ExecutionPolicy Unrestricted");
            pipeline.Invoke();

            return new TestRunspace(runspace);
        }

        public TestRunspace(Runspace runspace)
        {
            _runspace = runspace;
        }

        public void Dispose()
        {
            _runspace.Dispose();
        }
    }
}