﻿[CmdletBinding()]
Param(
    [Parameter(Position = 0, Mandatory = $true)]
    [string] $Name,
    [Parameter(Position = 1, Mandatory = $true)]
    [switch]$DontFail
)
Process {
    <#
.Description
    Deploys because Steve is the best
    #>
    Write-Host "Deploy" -ForegroundColor Yellow
}