﻿using System.Collections.Generic;
using PowerShellRunner.Projects;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Projects.GetAllOptions.GivenAProjectionCollection.WithAMatchingCurrentProject
{
    public class WhenGettingAllOptions
    {
        private readonly AllOptions _result;

        public WhenGettingAllOptions()
        {
            var subject = TestProject.New("C:/projects/arg");
            _result = subject.GetAllOptions();
        }

        [Fact]
        public void ThenAllTheProjectsAreReturned()
        {
            _result.Projects.ShouldBe(new List<string> { "arg", "default", "opt" });
        }

        [Fact]
        public void ThenTheDistinctMatchingAndDefaultOptionsAreReturned()
        {
            _result.Aliases.ShouldBe(new List<string> { "l", "loc", "n" });
        }

        [Fact]
        public void ThenTheDistinctMatchingAndTheDefaultCommandsAreReturned()
        {
            _result.Commands.ShouldBe(new List<string> { "build", "deploy" });
        }

        [Fact]
        public void ThenTheDistinctMatchingAndTheDefaultParametersAreReturned()
        {
            _result.Parameters.ShouldBe(new List<string> { "Location", "Name", "Quick" });
        }

        [Fact]
        public void ThenTheDistinctMatchingAndTheDefaultValueParametersAreReturned()
        {
            _result.ValueParameters.ShouldBe(new List<string> { "Location", "Name" });
        }
    }
}