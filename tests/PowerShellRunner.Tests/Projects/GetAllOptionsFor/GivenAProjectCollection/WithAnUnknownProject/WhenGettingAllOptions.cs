﻿using System.Collections.Generic;
using PowerShellRunner.Projects;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Projects.GetAllOptionsFor.GivenAProjectCollection.WithAnUnknownProject
{
    public class WhenGettingAllOptions
    {
        private readonly AllOptions _result;

        public WhenGettingAllOptions()
        {
            var subject = TestProject.New();
            _result = subject.GetOptionsFor("unknown");
        }

        [Fact]
        public void ThenAllTheProjectsAreReturned()
        {
            _result.Projects.ShouldBe(new List<string> { "arg", "default", "opt" });
        }

        [Fact]
        public void ThenOnlyTheDefaultOptionsAreReturned()
        {
            _result.Aliases.ShouldBe(new List<string> { "n" });
        }

        [Fact]
        public void ThenOnlyTheDefaultCommandsAreReturned()
        {
            _result.Commands.ShouldBe(new List<string> { "build" });
        }

        [Fact]
        public void ThenOnlyTheDefaultParametersAreReturned()
        {
            _result.Parameters.ShouldBe(new List<string> { "Name" });
        }

        [Fact]
        public void ThenOnlyTheDefaultValueParametersAreReturned()
        {
            _result.ValueParameters.ShouldBe(new List<string> { "Name" });
        }
    }
}