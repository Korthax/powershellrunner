﻿using System.Collections.Generic;
using PowerShellRunner.Projects;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Projects.TryGetCommand.GivenAProjectCollection.WithACurrentProject.AndAMatchingCommand
{
    public class WhenTryingToGetTheCommand
    {
        private readonly Command _command;
        private readonly bool _result;

        public WhenTryingToGetTheCommand()
        {
            var subject = TestProject.New("C:/projects/opt");
            _result = subject.TryGetCommand("dance", out _command);
        }

        [Fact]
        public void ThenTheGetIsSuccessful()
        {
            _result.ShouldBe(true);
        }

        [Fact]
        public void ThenThecorrectCommandIsReturned()
        {
            _command.Name.ShouldBe("dance");
            _command.ScriptFile.ShouldBe("C:/projects/opt/scripts/dance.ps1");
            _command.Aliases.ShouldBe(new Dictionary<string, string> { ["n"] = "Name" });
        }
    }
}