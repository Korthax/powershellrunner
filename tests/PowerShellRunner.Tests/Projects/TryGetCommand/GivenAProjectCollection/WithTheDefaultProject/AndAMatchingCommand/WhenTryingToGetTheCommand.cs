﻿using PowerShellRunner.Projects;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Projects.TryGetCommand.GivenAProjectCollection.WithTheDefaultProject.AndAMatchingCommand
{
    public class WhenTryingToGetTheCommand
    {
        private readonly Command _command;
        private readonly bool _result;

        public WhenTryingToGetTheCommand()
        {
            var subject = TestProject.New();
            _result = subject.TryGetCommand("build", out _command);
        }

        [Fact]
        public void ThenTheGetIsSuccessful()
        {
            _result.ShouldBe(true);
        }

        [Fact]
        public void ThenThecorrectCommandIsReturned()
        {
            _command.Name.ShouldBe("build");
            _command.ScriptFile.ShouldBe("C:/projects/psr/scripts/build.ps1");
        }
    }
}