﻿using PowerShellRunner.Projects;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Projects.TryGetCommand.GivenAProjectCollection.WithAKnownProject.AndAMatchingCommand
{
    public class WhenTryingToGetTheCommand
    {
        private readonly Command _command;
        private readonly bool _result;

        public WhenTryingToGetTheCommand()
        {
            var subject = TestProject.New("C:/projects/opt");
            _result = subject.TryGetCommand("deploy", out _command, "arg");
        }

        [Fact]
        public void ThenTheGetIsSuccessful()
        {
            _result.ShouldBe(true);
        }

        [Fact]
        public void ThenThecorrectCommandIsReturned()
        {
            _command.Name.ShouldBe("deploy");
            _command.ScriptFile.ShouldBe("C:/projects/arg/scripts/deploy.ps1");
        }
    }
}