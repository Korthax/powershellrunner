﻿using PowerShellRunner.Projects;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Projects.TryGetCommand.GivenAProjectCollection.WithAKnownProject.AndNoMatchingCommand
{
    public class WhenTryingToGetTheCommand
    {
        private readonly Command _command;
        private readonly bool _result;

        public WhenTryingToGetTheCommand()
        {
            var subject = TestProject.New("C:/projects/opt");
            _result = subject.TryGetCommand("dance", out _command, "arg");
        }

        [Fact]
        public void ThenTheGetFails()
        {
            _result.ShouldBe(false);
        }

        [Fact]
        public void ThenTheCommandIsNull()
        {
            _command.ShouldBeNull();
        }
    }
}