﻿using Moq;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.Commands;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Environment.GivenAnEnvironment.WithAKnownCommand.WithHelp
{
    public class WhenGettingTheCommandInfo
    {
        private readonly ICommandInfo _result;

        public WhenGettingTheCommandInfo()
        {
            using (TestRunspace.Create())
            {
                var subject = new EnvironmentHelper(new Mock<ILogger>().Object);
                _result = subject.GetCommandInfoFor($"{System.Environment.CurrentDirectory}/TestHelpers/Scripts/basic-help.ps1");
            }
        }

        [Fact]
        public void ThenTheParametersArePopulated()
        {
            _result.Parameters.ShouldNotBeEmpty();
        }

        [Fact]
        public void ThenTheHelpDescriptionIsPopulated()
        {
            _result.Help.Description.ShouldBe("Deploys because Steve is the best");
        }

        [Fact]
        public void ThenTheHelpSynopsisIsPopulated()
        {
            _result.Help.Synopsis.ShouldBe("Steve is the best");
        }
    }
}