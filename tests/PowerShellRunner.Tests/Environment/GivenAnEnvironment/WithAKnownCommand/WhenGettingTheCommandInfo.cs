﻿using Moq;
using PowerShellRunner.Environment;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Environment.GivenAnEnvironment.WithAKnownCommand
{
    public class WhenGettingTheCommandInfo
    {
        [Fact]
        public void ThenTheParametersArePopulated()
        {
            using (TestRunspace.Create())
            { 
                var subject = new EnvironmentHelper(new Mock<ILogger>().Object);
                var result = subject.GetCommandInfoFor($"{System.Environment.CurrentDirectory}/TestHelpers/test.ps1");

                result.Parameters.ShouldNotBeEmpty();
            }
        }
    }
}