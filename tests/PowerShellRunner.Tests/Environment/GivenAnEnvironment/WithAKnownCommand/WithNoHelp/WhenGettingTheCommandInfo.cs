﻿using Moq;
using PowerShellRunner.Environment;
using PowerShellRunner.Environment.Commands;
using PowerShellRunner.Tests.TestHelpers;
using Shouldly;
using Xunit;

namespace PowerShellRunner.Tests.Environment.GivenAnEnvironment.WithAKnownCommand.WithNoHelp
{
    public class WhenGettingTheCommandInfo
    {
        private readonly ICommandInfo _result;

        public WhenGettingTheCommandInfo()
        {
            using (TestRunspace.Create())
            {
                var subject = new EnvironmentHelper(new Mock<ILogger>().Object);
                _result = subject.GetCommandInfoFor($"{System.Environment.CurrentDirectory}/TestHelpers/Scripts/no-help.ps1");
            }
        }

        [Fact]
        public void ThenTheParametersArePopulated()
        {
            _result.Parameters.ShouldNotBeEmpty();
        }

        [Fact]
        public void ThenTheHelpDescriptionIsNull()
        {
            _result.Help.Description.ShouldBeNull();
        }

        [Fact]
        public void ThenTheHelpSynopsisIsNull()
        {
            _result.Help.Synopsis.ShouldBeNull();
        }
    }
}